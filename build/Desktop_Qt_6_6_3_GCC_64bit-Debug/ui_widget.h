/********************************************************************************
** Form generated from reading UI file 'widget.ui'
**
** Created by: Qt User Interface Compiler version 6.6.3
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_WIDGET_H
#define UI_WIDGET_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPlainTextEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QToolButton>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Widget
{
public:
    QGridLayout *gridLayout;
    QLineEdit *lePort;
    QHBoxLayout *horizontalLayout_3;
    QPlainTextEdit *pteMsg;
    QPushButton *pteSend;
    QLineEdit *leHost;
    QHBoxLayout *horizontalLayout;
    QPushButton *pbConnect;
    QPushButton *pbDisconnect;
    QPushButton *pbClear;
    QPlainTextEdit *pteMessage;
    QToolButton *toolButton;

    void setupUi(QWidget *Widget)
    {
        if (Widget->objectName().isEmpty())
            Widget->setObjectName("Widget");
        Widget->resize(782, 505);
        gridLayout = new QGridLayout(Widget);
        gridLayout->setObjectName("gridLayout");
        lePort = new QLineEdit(Widget);
        lePort->setObjectName("lePort");
        QSizePolicy sizePolicy(QSizePolicy::Policy::Fixed, QSizePolicy::Policy::Minimum);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(lePort->sizePolicy().hasHeightForWidth());
        lePort->setSizePolicy(sizePolicy);

        gridLayout->addWidget(lePort, 1, 1, 1, 1);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setObjectName("horizontalLayout_3");
        pteMsg = new QPlainTextEdit(Widget);
        pteMsg->setObjectName("pteMsg");

        horizontalLayout_3->addWidget(pteMsg);

        pteSend = new QPushButton(Widget);
        pteSend->setObjectName("pteSend");
        QSizePolicy sizePolicy1(QSizePolicy::Policy::Minimum, QSizePolicy::Policy::Expanding);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(pteSend->sizePolicy().hasHeightForWidth());
        pteSend->setSizePolicy(sizePolicy1);

        horizontalLayout_3->addWidget(pteSend);


        gridLayout->addLayout(horizontalLayout_3, 4, 0, 1, 2);

        leHost = new QLineEdit(Widget);
        leHost->setObjectName("leHost");

        gridLayout->addWidget(leHost, 1, 0, 1, 1);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName("horizontalLayout");
        horizontalLayout->setSizeConstraint(QLayout::SetNoConstraint);
        pbConnect = new QPushButton(Widget);
        pbConnect->setObjectName("pbConnect");

        horizontalLayout->addWidget(pbConnect);

        pbDisconnect = new QPushButton(Widget);
        pbDisconnect->setObjectName("pbDisconnect");

        horizontalLayout->addWidget(pbDisconnect);

        pbClear = new QPushButton(Widget);
        pbClear->setObjectName("pbClear");

        horizontalLayout->addWidget(pbClear);


        gridLayout->addLayout(horizontalLayout, 0, 0, 1, 2);

        pteMessage = new QPlainTextEdit(Widget);
        pteMessage->setObjectName("pteMessage");

        gridLayout->addWidget(pteMessage, 3, 0, 1, 2);

        toolButton = new QToolButton(Widget);
        toolButton->setObjectName("toolButton");

        gridLayout->addWidget(toolButton, 2, 0, 1, 2);

        QWidget::setTabOrder(lePort, leHost);
        QWidget::setTabOrder(leHost, pbConnect);
        QWidget::setTabOrder(pbConnect, pbDisconnect);
        QWidget::setTabOrder(pbDisconnect, pbClear);
        QWidget::setTabOrder(pbClear, pteMessage);
        QWidget::setTabOrder(pteMessage, pteMsg);
        QWidget::setTabOrder(pteMsg, pteSend);

        retranslateUi(Widget);

        QMetaObject::connectSlotsByName(Widget);
    } // setupUi

    void retranslateUi(QWidget *Widget)
    {
        Widget->setWindowTitle(QCoreApplication::translate("Widget", "Widget", nullptr));
        lePort->setText(QCoreApplication::translate("Widget", "443", nullptr));
        pteMsg->setPlainText(QCoreApplication::translate("Widget", "GET / HTTP/1.1\n"
"Host: www.naver.com\n"
"\n"
"", nullptr));
        pteSend->setText(QCoreApplication::translate("Widget", "send", nullptr));
        leHost->setText(QCoreApplication::translate("Widget", "www.naver.com", nullptr));
        pbConnect->setText(QCoreApplication::translate("Widget", "Connect", nullptr));
        pbDisconnect->setText(QCoreApplication::translate("Widget", "Disconnect", nullptr));
        pbClear->setText(QCoreApplication::translate("Widget", "Clear", nullptr));
        toolButton->setText(QCoreApplication::translate("Widget", "...", nullptr));
    } // retranslateUi

};

namespace Ui {
    class Widget: public Ui_Widget {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_WIDGET_H
